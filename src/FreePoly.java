import java.util.HashSet;
import java.util.LinkedList;

public class FreePoly{
	Polyomino polyomino;
	HashSet<Polyomino> representants;
	public final static int N=8;
	
	
	public FreePoly(Polyomino poly) {
		this.polyomino = poly;
		representants = new HashSet<Polyomino>();
	}
	
	public void generateRepresentants() {
		this.representants.add(polyomino.clone());
		Polyomino temp = polyomino.clone();
		for (int i = 0 ; i< 4 ; i++) {
			temp.rotation(0, 0);
			temp.recentre();
			this.representants.add(temp.clone());
		}
		temp.reflectionX(0);
		for (int i = 0 ; i< 4 ; i++) {
			temp.rotation(0, 0);
			temp.recentre();
			this.representants.add(temp.clone());
		}
	}
	
	
	/**
	 * Modifie en place list en ne gardant qu'un seul repr�sentant 
	 * par classe d'�quivalence (on quotiente par O_2(R)) 
	 * Pour gagner en efficacit�, on ne g�n�re les repr�sentants du free polyomino 
	 * que lorsqu'il va �tre ajout� � la liste
	 */
	public static void filtreFreePolyominoes(LinkedList<Polyomino> list) {
		int n = list.size();
		LinkedList<FreePoly> freeList = new LinkedList<FreePoly>();
		for (int i = 0 ; i< n ; i++) {
			FreePoly poly = new FreePoly(list.remove());
			// La fonction equals �tant asym�trique, les testent peuvent avoir lieu
			// sans que this n'est g�n�r� ses repr�sentatns
			if (!freeList.contains(poly)) {
				poly.generateRepresentants();
				freeList.add(poly);
			}
		}
		for (FreePoly freePoly : freeList) {
			list.addFirst(freePoly.polyomino);
		}
	}
	
	
	@Override
	public boolean equals(Object obj) {
		FreePoly that = (FreePoly) obj;
		for (Polyomino poly : that.representants) {
			if (this.polyomino.equals(poly))
				return true;
		}
		return false;
	}
	
	
	public static void main(String[] args) {
		LinkedList<Polyomino> l = GeneratePoly.generatePoly(8);
		GenerateNaive.filtreAire(l, 8);
		FreePoly.filtreFreePolyominoes(l);
		Image2d img = new Image2d(400, 300);
		Image2dViewer view = new Image2dViewer(img);
		int i = 0;
		int j = 1;
		for (Polyomino poly : l) {
			poly.translate(++i*8, j*8);
			if (i == 24) {
				j++;
				i = 0;
			}
			poly.draw(img);
		}
		System.out.println(l.size());
	}
	
	
	
	
	
	
	
}
