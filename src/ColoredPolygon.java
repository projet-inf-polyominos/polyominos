import java.awt.*;
import java.util.LinkedList;

public class ColoredPolygon {
	/**
	 * paramtres par dfaut sur lesquels on peut jouer Sinon que des constructeurs
	 */
	final static int SIZE = 20;
	final static Color defaut = Color.red;

	final static Color[] colors = { Color.red, Color.green, Color.blue, Color.black, Color.orange, Color.cyan,
			Color.gray, Color.MAGENTA };
	public Polygon polygon;
	public Color color;

	public ColoredPolygon(int[] x, int[] y, Color color) {
		this.polygon = new Polygon(x, y, x.length);
		this.color = color;
	}

	public ColoredPolygon(int[] x, int[] y) {
		this.polygon = new Polygon(x, y, x.length);
		this.color = color;
	}

	public ColoredPolygon(LinkedList<Coord> coordinates) {

		int[] coordx = new int[coordinates.size()];
		int[] coordy = new int[coordx.length];
		for (int i = 0; i < coordx.length; i++) {
			coordx[i] = coordinates.get(i).x * SIZE;
			coordy[i] = coordinates.get(i).y * SIZE;
		}
		this.polygon = new Polygon(coordx, coordy, coordx.length);
	}

	public ColoredPolygon(LinkedList<Coord> coordinates, Color color) {
		this.color = color;
		int[] coordx = new int[coordinates.size()];
		int[] coordy = new int[coordx.length];
		for (int i = 0; i < coordx.length; i++) {
			coordx[i] = coordinates.get(i).x * SIZE;
			coordy[i] = coordinates.get(i).y * SIZE;
		}
		this.polygon = new Polygon(coordx, coordy, coordx.length);
	}

	public static LinkedList<ColoredPolygon> buildPicture(Polyomino poly) {
		LinkedList<ColoredPolygon> squares = new LinkedList<ColoredPolygon>();
		// int k = (int) (Math.random() * colors.length);
		Color locColor = Color.getHSBColor((float) Math.random(), (float) Math.random(), (float) Math.random());
		//Color locColor = defaut;
		for (Coord coord : poly.coordinates) {
			squares.add(new ColoredPolygon(
					new int[] { coord.x * SIZE, coord.x * SIZE, (coord.x + 1) * SIZE, (coord.x + 1) * SIZE },
					new int[] { coord.y * SIZE, (coord.y + 1) * SIZE, (coord.y + 1) * SIZE, coord.y * SIZE },
					locColor));
		}
		return squares;
	}

	public static LinkedList<Edge> buildEdges(Polyomino poly) {
		LinkedList<Edge> listEdge = new LinkedList<Edge>();
		for (Coord coord : poly.coordinates) {
			if (!poly.coordinates.contains(Coord.sum(coord, Polyomino.NORTH))) {
				listEdge.add(new Edge(coord.x * SIZE, (coord.y+1) * SIZE, (coord.x+1) * SIZE,  (coord.y+1) * SIZE));
			}
			if (!poly.coordinates.contains(Coord.sum(coord, Polyomino.EAST))) {
				listEdge.add(new Edge((coord.x+1) * SIZE, (coord.y) * SIZE, (coord.x+1) * SIZE,  (coord.y+1) * SIZE));
			}
			if (!poly.coordinates.contains(Coord.sum(coord, Polyomino.SOUTH))) {
				listEdge.add(new Edge(coord.x * SIZE, coord.y * SIZE, (coord.x+1) * SIZE,  (coord.y) * SIZE));
			}
			if (!poly.coordinates.contains(Coord.sum(coord, Polyomino.WEST))) {
				listEdge.add(new Edge(coord.x * SIZE, coord.y * SIZE, coord.x * SIZE,  (coord.y+1) * SIZE));
			}
		}
		

		return listEdge;
	}

	public static void main(String[] args) {
		Polyomino poly = Polyomino.bigSquare(2);
		poly.translate(1, 1);
		LinkedList<Edge> list = buildEdges(poly);
		for (Edge e : list)
			System.out.println(e);
	}

}
