import java.util.Collection;
import java.util.LinkedList;

public class GenerateNaive2 {

	/**
	 * Une autre m�thode consiste � faire pousser les polyominos n fois autour d'une
	 * case
	 * Pour le test d'�galit� entre deux polyominos, on recentre chaque nouveau polyomino
	 * pour qu'il soit en appui nord est sur le plan
	 */
	public static LinkedList<Polyomino> generateAllFixedPolyominoes2(int n) {
		LinkedList<Polyomino> res = new LinkedList<Polyomino>();
		res.add(new Polyomino(new Coord()));
		for (int nbTile = 1; nbTile < n; nbTile++) {
			// nbTile est le nombre de cases des polyominos de res en d�but de boucle
			expand(res, nbTile);
		}
		return res;
	}

	/**
	 * expand prend la liste des polyomino et remplace chaque polyominos par ses
	 * versions "�tenduees" 
	 * C'est � dire l'ensemble des polyominos que l'on peut
	 * construire avec le polyomino d'origine et une case en plus
	 */
	public static void expand(LinkedList<Polyomino> list, int nbTile) {
		int m = list.size();
		// pour chaque polyomino d'origine
		for (int j = 0; j < m; j++) {
			Polyomino poly = list.removeFirst();
			// pour chacune de ces cases
			for (Coord tile : poly.coordinates) {
				// pour chaque direction
				for (int d = 0; d < 4; d++) {
					Coord c = Coord.sum(tile, Polyomino.DIRS[d]);
					// on regarde si on peut ajouter une case
					if (!poly.coordinates.contains(c)) {
						Polyomino temp = poly.clone();
						temp.add(c);
						temp.recentre();;
						if (!list.contains(temp))
							list.addLast(temp);
					}
				}
			}
		}
	}



	public static void main(String[] args) {
		Polyomino surface1 = new Polyomino("[(0,0), (0,1), (0,2), (0,3)]");
		LinkedList<Polyomino> l = generateAllFixedPolyominoes2(8);
		/*for (Polyomino poly : l) {
			Image2d img = new Image2d(100, 100);
			Image2dViewer view = new Image2dViewer(img);
			poly.draw(img);
			System.out.println(poly);
		}*/
		System.out.println(l.size());
	}
}
