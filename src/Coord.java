import java.util.LinkedList;

public class Coord {
	int x;
	int y;

	Coord(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	Coord() {
		this.x = 0;
		this.y = 0;
	}

	public int distAuCarre(Coord that) {
		return (this.x - that.x) * (this.x - that.x) + (this.y - that.y) * (this.y - that.y);
	}
	
	public void sum(Coord that) {
		this.x += that.x;
		this.y += that.y;
	}
	
	public void sum(int xT, int yT) {
		this.x += xT;
		this.y += yT;
	}
	
	
	public static Coord sum(Coord p1, Coord p2) {
		return new Coord(p1.x + p2.x, p1.y + p2.y);
	}
	
	public LinkedList<Coord> sumVector(LinkedList<Coord> l) {
		LinkedList<Coord> res = new LinkedList<Coord>();
		for (Coord coord : l)
			res.add(sum(this, coord));
		return res;
	}
	
	public void dilate(int alpha, Coord origin) {
		x =origin.x + (x - origin.x) * alpha;
		y =origin.y + (y - origin.y) * alpha;
	}	
	
	@Override
	public String toString() {
		return "("+x+","+y+")";
	}
	
	@Override
	public boolean equals(Object obj) {
		Coord that = (Coord) obj;
		return this.x == that.x && this.y == that.y;
	}
	
	@Override
	public int hashCode() {
		return x * 17 + y *29;
	}
	
	public Coord clone() {
		return new Coord(x,y);
	}
	
}