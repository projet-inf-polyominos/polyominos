import java.util.LinkedList;

public class CoverDilate5 {
	public static final Polyomino poly1 = new Polyomino("[(0,0), (0,1), (0,2), (1,2), (1,0)]");
	public static final Polyomino poly2 = new Polyomino("[(0,0), (0,1), (0,2), (0,3), (0,4)]");
	public static final Polyomino poly3 = new Polyomino("[(2,0), (2,1), (2,2), (1,2), (0,2)]");
	public static final Polyomino poly4 = new Polyomino("[(1,0), (1,1), (0,1), (1,2), (2,1)]");
	public static final Polyomino poly5 = new Polyomino("[(1,0), (1,1), (0,1), (0,2), (1,2)]");
	public static final Polyomino poly6 = new Polyomino("[(2,0), (2,1), (1,1), (1,2), (0,2)]");
	public static final Polyomino poly7 = new Polyomino("[(2,0), (2,1), (1,1), (0,1), (3,0)]");
	public static final Polyomino poly8 = new Polyomino("[(2,0), (2,1), (1,1), (0,1), (3,1)]");
	public static final Polyomino poly9 = new Polyomino("[(2,0), (2,1), (1,1), (0,1), (2,2)]");
	public static final Polyomino poly10 = new Polyomino("[(2,0), (2,1), (1,1), (0,1), (1,2)]");
	public static final Polyomino poly11 = new Polyomino("[(2,0), (2,1), (1,1), (0,1), (0,2)]");
	public static final Polyomino poly12 = new Polyomino("[(3,0), (3,1), (2,1), (1,1), (0,1)]");
	public static final Polyomino[] tab = { poly1, poly2, poly3, poly4, poly5, poly6, poly7, poly8, poly9, poly10,
			poly11, poly12 };

	public static void main(String[] args) {
		for (Polyomino pattern : tab) {
			Polyomino surface = pattern.clone();
			surface.rotation(0, 0);
			surface.dilatation(4);
			surface.recentre();

			FreePoly fp = new FreePoly(pattern.clone());
			fp.generateRepresentants();

			LinkedList<Polyomino> polyominoSet = new LinkedList<Polyomino>();
			for (Polyomino poly : fp.representants) {
				polyominoSet.add(poly);
			}
			for (Polyomino poly : polyominoSet) {
				poly.recentre();
			}

			Tiling tiling = new Tiling(polyominoSet, surface);

			LinkedList<LinkedList<Polyomino>> covers = tiling.polyCover();
			if (covers.size() > 0) {
				Image2d img = new Image2d(300, 220);
				Image2dViewer view = new Image2dViewer(img);

				for (Polyomino poly : covers.get(0)) {
					poly.draw(img);
				}
			}
		}
	}
}
