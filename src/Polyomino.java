import java.awt.*;
import java.util.Collection;
import java.util.LinkedList;

public class Polyomino {
	// permet de simplier pas mal la relecture
	public final static Coord NORTH = new Coord(0, 1), EAST = new Coord(1, 0), SOUTH = new Coord(0, -1),
			WEST = new Coord(-1, 0);
	public final static Coord[] DIRS = new Coord[] { NORTH, EAST, SOUTH, WEST };
	final static Polygon pattern = new Polygon(new int[] { 0, 0, 1, 1 }, new int[] { 0, 1, 1, 0 }, 4);

	// champ dynamique
	public LinkedList<Coord> coordinates;
	int nbPat; // nb patterns

	public Polyomino(LinkedList<Coord> coordinates) {
		this.coordinates = coordinates;
		this.nbPat = coordinates.size();
	}

	public Polyomino(String line) {
		this.coordinates = TXTPolyomino.readPolyomino(line);
		this.nbPat = coordinates.size();
	}

	public Polyomino(Coord tile) {
		this.coordinates = new LinkedList<Coord>();
		coordinates.add(tile);
		this.nbPat = 1;
	}

	public void draw(Image2d img) {
		img.addPolyomino(this);
	}

	public void translate(int xT, int yT) {
		for (Coord coord : coordinates) {
			coord.sum(xT, yT);
		}
	}

	// coordonnees centre rotation, angle = pi/2
	public void rotation(int x0, int y0) {
		for (Coord coord : this.coordinates) {
			// swap
			int a = coord.x;
			coord.x = x0 + (coord.y - y0);
			coord.y = y0 - (a - x0);
		}
	}

	public void reflectionX(int y0) {
		for (Coord coord : coordinates) {
			coord.y = -1 * (coord.y - y0) + y0;
		}
	}

	public void reflectionY(int x0) {
		for (Coord coord : coordinates) {
			coord.x = -1 * (coord.x - x0) + x0;
		}
	}

	public static Polyomino bigSquare(int alpha) {
		LinkedList<Coord> res = new LinkedList<Coord>();
		Coord temp = new Coord();
		for (int i = 0; i < alpha; i++) {
			for (int j = 0; j < alpha; j++) {
				res.add(temp.clone());
				temp.sum(EAST);
			}
			temp.x = 0;
			temp.sum(NORTH);
		}
		return new Polyomino(res);
	}
	
	public static Polyomino rectangle(int width, int weight) {
		LinkedList<Coord> res = new LinkedList<Coord>();
		Coord temp = new Coord();
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < weight; j++) {
				res.add(temp.clone());
				temp.sum(EAST);
			}
			temp.x = 0;
			temp.sum(NORTH);
		}
		return new Polyomino(res);
	}

	public void dilatation(int alpha) {
		Coord origin = this.coordinates.get(0), temp;
		LinkedList<Coord> bigPattern = bigSquare(alpha).coordinates;
		for (int k = 0; k < nbPat; k++) {
			temp = coordinates.remove(0);
			temp.dilate(alpha, origin);
			this.coordinates.addAll(temp.sumVector(bigPattern));
		}
		this.nbPat *= alpha * alpha;
	}
	
	//assuming 
	public static Polyomino merge(Polyomino poly1, Polyomino poly2) {
		Polyomino res = poly1.clone();
		for (Coord tile : poly2.coordinates) {
			if (!res.coordinates.contains(tile))
				res.add(tile.clone());
		}
		return res;
	}

	public Coord remove(int i) {
		nbPat--;
		return this.coordinates.remove(i);
	}

	public boolean remove(Coord tile) {
		if (this.coordinates.remove(tile)) {
			nbPat--;
			return true;
		}
		return false;
	}

	public void add(int i, Coord tile) {
		// assuming it's adjacent
		nbPat++;
		this.coordinates.add(i, tile);
	}

	public void add(Coord tile) {
		// assuming it's adjacent
		nbPat++;
		this.coordinates.add(tile);
	}
	
	public Coord get(int i) {
		return coordinates.get(i);
	}

	public Polyomino clone() {
		LinkedList<Coord> l = new LinkedList<Coord>();
		for (Coord tile : this.coordinates) {
			l.add(tile.clone());
		}
		return new Polyomino(l);
	}
	
	// met le carre d'indice 0 a la position donnee 
	public void setPosition(int xT, int yT) {
		this.translate(xT - this.coordinates.get(0).x, yT - this.coordinates.get(0).y);
	}
	
	public void setPosition(Coord coord) {
		setPosition(coord.x, coord.y);
	}
	
	/**
	 * Translate le polyomino pour qu'il soit en haut a gauche
	 */
	public void recentre() {
		int xmin = this.coordinates.get(0).x, ymin = this.coordinates.get(0).y;
		for (int i = 1; i < coordinates.size(); i++) { // n,bPat fix� � 1 avec genratePoly
			Coord coord = this.coordinates.get(i);
			xmin = Math.min(xmin, coord.x);
			ymin = Math.min(ymin, coord.y);
		}
		this.translate(-xmin, -ymin);
	}
	
	// renvoie les ordonnees et abscisses maximales des carres du polyomino
	// sert dans le tiling (efficace que si le polyomino est recentr�)
	public Coord getMaxXAndY() {
		int maxX = 0;
		int maxY = 0;
		for(Coord square : this.coordinates) {
			if(square.x > maxX) {
				maxX = square.x;
			}
			if(square.y > maxY) {
				maxY = square.y;
			}
		}
		return new Coord(maxX, maxY);
	}
	
	/**
	 * Equivalent vectoriel de recentrer
	 */
	public static void recentre(Collection<Polyomino> hset) {
		for (Polyomino poly : hset)
			poly.recentre();
	}
	
	public boolean contenuDans(Polyomino surface) {
		for (Coord tile : this.coordinates) {
			if (!surface.coordinates.contains(tile))
				return false;
		}
		return true;
	}
	

	@Override
	public String toString() {
		String res = "";
		for (Coord c : this.coordinates)
			res += ", " + c;
		if (res.length() > 1)
			res = res.substring(2);
		return "[" + res + "]";
	}

	@Override
	public boolean equals(Object obj) {
		Polyomino that = (Polyomino) obj;
		if (this.nbPat != that.nbPat)
			return false;
		// injectivit + galit de card
		return this.coordinates.containsAll(that.coordinates);
	}

	@Override
	/**
	 * Il faut une fonction qui renvoie le mme rsultat par permutation des sommets
	 */
	public int hashCode() {
		int sum = 0;
		for (Coord coord : this.coordinates)
			sum += coord.hashCode();
		return sum;
	}

	public static void main(String[] args) {
		Polyomino poly1 = rectangle(3, 7);
		Polyomino poly2 = new Polyomino("[(1,1), (0,0), (1,0)]");
		System.out.println(poly1.equals(poly2));
		System.out.println(poly1);
		Polyomino poly3 = merge(poly2,poly1);
		System.out.println(poly3);
		Image2d img = new Image2d(100, 100);
		Image2dViewer view = new Image2dViewer(img);
		poly1.draw(img);
	}



}
