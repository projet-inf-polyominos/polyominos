import java.util.LinkedList;
import java.io.IOException;
import java.util.Stack;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Collections;
import java.awt.Color;

public class GeneratePoly {
	// liste a construire
	public LinkedList<Polyomino> fixedPolyList;
	// free = points ni reachable ni occupied ni border
	int p;
	int width = 500;
	int height = 500;	
	
	public GeneratePoly(int p) {
		this.p = p;
		this.fixedPolyList = new LinkedList<Polyomino>();
	}
	
	// methode a appeler pour l'appel
	public void generateFixedPoly() {
		boolean[][] free = new boolean[2*p + 2][p];
		for(int i = 0; i < 2*p + 2; i ++) {
			for(int j = 0; j < p; j++) {
				free[i][j] = true;
			}
		}
		// premier point = non free
		free[p][0] = false;
		
		Stack<Coord> stackInt = new Stack<Coord>();
		stackInt.push(new Coord(0,0));
		LinkedList<Coord> couples = new LinkedList<Coord>();
		Polyomino poly_null = new Polyomino(couples);
		
		generateFixedPolyRoutine(stackInt, poly_null, free);
		
		for (Polyomino poly : this.fixedPolyList)
			poly.nbPat = poly.coordinates.size();
	}
	
	public void generateFixedPolyRoutine(Stack<Coord> untried, Polyomino polyPere, boolean[][] free) {
		while(!untried.isEmpty()) {
			Coord pair = untried.pop();
			
			LinkedList<Coord> currentSquares = new LinkedList<Coord>();
			for	(Coord coord : polyPere.coordinates)
				currentSquares.add(coord.clone());
			currentSquares.add(pair.clone());
			
			Polyomino currentPoly = new Polyomino(currentSquares);
			
			this.fixedPolyList.add(currentPoly);
						
			if(currentSquares.size() < this.p) {
				int x = pair.x;
				int y = pair.y;
				
				// on copie les references aux colonnes 
				boolean [][] newFree = new boolean[free.length][];
				for(int i = 0; i < free.length; i++)
				    newFree[i] = free[i].clone();
				
				int nbNew = 0;
				
				Coord est = new Coord(x + 1, y);
				Coord sud = new Coord(x, y + 1);
				// oui l'axe y est invers�...
				Coord nord = new Coord(x, y - 1);
				Coord ouest = new Coord(x - 1, y);
				
				// attention aux bords
				// on teste si le point est bien free, cad pas reachable avant ni occupe
				if(free[p - est.x][est.y]) {
					// le point n'est pas free dans l'instance recursive suivante ! il est reachable !
					newFree[p - est.x][est.y] = false;
					untried.push(est);
					nbNew ++;
				}
				if(free[p - sud.x][sud.y]) {
					newFree[p - sud.x][sud.y] = false;
					untried.push(sud);
					nbNew ++;
				}
				if(nord.y >= 0 && (y != 1 || x >= 0) && free[p - nord.x][nord.y]) {
					newFree[p - nord.x][nord.y] = false;
					untried.push(nord);
					nbNew ++;
				}
				// pas a gauche de lorigine sur laxe
				if((y > 0) && !currentSquares.contains(ouest) && free[p - ouest.x][ouest.y]) {
					newFree[p - ouest.x][ouest.y] = false;
					untried.push(ouest);
					nbNew ++;
				}
				
				generateFixedPolyRoutine((Stack<Coord>) untried.clone(), currentPoly, newFree);	
				
				// on enleve les nouveaux voisins
				for(int i = 0; i < nbNew; i++) {
					untried.pop();
				}
			}
		}	
	}
	
	/**
	 * retourne la liste de tous les fixed polyominos de taille n
	 */
	public static LinkedList<Polyomino> generatePoly(int n) {
		GeneratePoly genp = new GeneratePoly(n);
		genp.generateFixedPoly();
		GenerateNaive.filtreAire(genp.fixedPolyList, n);
		Polyomino.recentre(genp.fixedPolyList);
		return genp.fixedPolyList;
	}
	
	public static void main(String[] args) {
		LinkedList<Polyomino> list = generatePoly(5);
		for (Polyomino poly : list)
			System.out.println(poly);
		System.out.println(list.size());
	}
	
	
	
	
}
