public class Edge {
	public final static int defaultWidth = 1;
	
	public int x1;
	public int x2;
	public int y1;
	public int y2;
	public int width;
	
	public Edge(int x1, int y1, int x2, int y2, int width) {
		this.x1 = x1;
		this.x2 = x2;
		this.y1 = y1;
		this.y2 = y2;
		this.width = width;
		
	}
	
	public Edge(int x1, int y1, int x2, int y2) {
		this.x1 = x1;
		this.x2 = x2;
		this.y1 = y1;
		this.y2 = y2;
		this.width = defaultWidth;
		
	}
	
	@Override
	public String toString() {
		return "[("+this.x1+", "+this.y1+"), ("+this.x2+", "+this.y2+")]";
	}
	
	public static void main(String[] args) {
		Edge edge = new Edge(1,1,2,1);
		System.out.println(edge);
	}
}
