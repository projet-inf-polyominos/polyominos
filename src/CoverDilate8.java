import java.util.LinkedList;

public class CoverDilate8 {

	public static void main(String[] args) {
		LinkedList<Polyomino> list = GeneratePoly.generatePoly(8);
		GenerateNaive.filtreAire(list, 8);
		FreePoly.filtreFreePolyominoes(list);
		Image2d img = new Image2d(80, 80);
		Image2dViewer view = new Image2dViewer(img);
		int i = 0, j = 1;
		for (Polyomino pattern : list) {

			Polyomino surface = pattern.clone();
			surface.rotation(0, 0);
			surface.dilatation(4);
			surface.recentre();

			FreePoly fp = new FreePoly(pattern.clone());
			fp.generateRepresentants();

			LinkedList<Polyomino> polyominoSet = new LinkedList<Polyomino>();
			for (Polyomino poly : fp.representants) {
				polyominoSet.add(poly);
			}
			for (Polyomino poly : polyominoSet) {
				poly.recentre();
			}

			Tiling tiling = new Tiling(polyominoSet, surface);

			LinkedList<LinkedList<Polyomino>> covers = tiling.polyCover();
			if (covers.size() > 0) {
				pattern.translate(++i*8, j*8);
				if (i == 5) {
					i=0;
					j++;
				}
				pattern.draw(img);
				//for (Polyomino poly : covers.get(0)) { poly.draw(img); }
				
			}

		}
		System.out.println("Terminated");
	}
}
