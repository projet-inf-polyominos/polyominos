import java.util.LinkedList;
import java.util.Stack;
import java.util.LinkedList;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.Collections;
import java.awt.Color;

import dancing_links.DataObject;
import dancing_links.DancingLinks;

public class Main {

	public static void main(String[] args) {		
		//LinkedList<Polyomino> pol = TXTPolyomino.polyominoFromTxt("D:\\poly.txt");
		for (int k = 0; k<4; k++) {
			for (int l=0; l < 4; l++) {
				Polyomino surface = Polyomino.bigSquare(8);
				for (int i =k ; i < 2+k ; i++)
					for (int j = l ; j<2+l;j++)
						surface.remove(new Coord(i,j));
				InjectiveTiling tiling = new InjectiveTiling(GeneratePoly.generatePoly(5), surface);
				tiling.polyCover();	
				Image2d img = new Image2d(100, 100);
				Image2dViewer view = new Image2dViewer(img);
				for (Polyomino poly : tiling.covers.get(0)) {
					poly.draw(img);
				}
			}
		}
	}

}
