import java.util.LinkedList;
import dancing_links.*;

public class Problem {
	public final static int[][] tab1 = new int[][] { { 2, 4, 5 }, { 0, 3, 6 }, { 1, 2, 5 }, { 0, 3 }, { 1, 6 },
			{ 3, 4, 6 } };
	// pour ce probl�me, deux solutions possibles
	public final static int[][] tab2 = new int[][] { { 2, 4, 5 }, { 0, 3, 6 }, { 1, 2, 5 }, { 0, 3 }, { 1, 6 }, { 4 } };

	public Boolean[][] coverMatrix;
	public LinkedList<Integer> setToCover;
	public LinkedList<LinkedList<Integer>> subsetsList;
	public int p; //nbLine
	public int[][] C;

	public Problem(int p, int[][] C) {// on se ram�ne � la couverture de [0, p[
		this.p = p;
		this.C = C;
		buildSetToCover();
		buildCoverMatrix();
	}	
	
	
	public void buildCoverMatrix() {
		int n = C.length;
		coverMatrix = new Boolean[n][];
		// initialisation de tout � faux
		for (int i = 0; i < n; i++) {
			coverMatrix[i] = new Boolean[p];
			for (int j = 0; j < p; j++) {
				coverMatrix[i][j] = false;
			}
		}
		subsetsList = new LinkedList<LinkedList<Integer>>();
		LinkedList<Integer> subset;
		for (int i = 0; i < n; i++) {
			subset = new LinkedList<Integer>();
			for (int x : C[i]) {
				subset.add(x);
				coverMatrix[i][x] = true;// -1 ??
			}
			subsetsList.add(subset);
		}
		
	}

	public void buildSetToCover() {
		
		setToCover = new LinkedList<Integer>();
		for (int i = 0; i < p; i++) {
			setToCover.add(i);
		}
		
		
	}

	// R�sous la couverture exact de fa�on na�ve sans heuristiques
	public LinkedList<LinkedList<LinkedList<Integer>>> exactCover1() {
		return ExactCover.findExactCoverVer1(setToCover, subsetsList);
	}
	
	// R�sous la couverture exacte de fa�on na�ve avec l'heuristique
	public LinkedList<LinkedList<LinkedList<Integer>>> exactCover2() {
		return ExactCover.findExactCoverVer2(setToCover, subsetsList);
	}
	
	public LinkedList<LinkedList<Integer>> indexExactCover() {
		DancingLinks dl = new DancingLinks(this.coverMatrix);
		return dl.indexExactCover();
	}
	
	/**
	 * une fois l'algo des liens dansant effectu�s, on n'a acc�s qu'aux indices des
	 * sous-ensemble � s�lectionner pour la couverture exacte
	 * Cette fonction permet d'avoirle r�sultat avec le contenu des sous-ensembles
	 */
	public LinkedList<LinkedList<LinkedList<Integer>>> exactCoverdl() {
		DancingLinks dl = new DancingLinks(this.coverMatrix);
		LinkedList<LinkedList<Integer>> coversIndexs = dl.indexExactCover();
		LinkedList<LinkedList<LinkedList<Integer>>> covers = new LinkedList<LinkedList<LinkedList<Integer>>>();
		for (LinkedList<Integer> coverIndex : coversIndexs) {
			LinkedList<LinkedList<Integer>> tempCover = new LinkedList<LinkedList<Integer>>();
			for (int index : coverIndex) {
				tempCover.add(subsetsList.get(index));
			}
			covers.add(tempCover);
		}
		return covers;
	}

	public static String displayExactCover(LinkedList<LinkedList<LinkedList<Integer>>> list) {
		String res = "", coverString, subsetString;
		for (LinkedList<LinkedList<Integer>> cover : list) {
			coverString = "";
			for (LinkedList<Integer> subset : cover) {
				subsetString = "";
				for (int x : subset)
					subsetString += ", " + x;
				if (subsetString.length() > 1)
					subsetString = subsetString.substring(2);
				subsetString = "{" + subsetString + "}";
				coverString += ", " + subsetString;
			}
			if (coverString.length() > 1)
				coverString = coverString.substring(2);
			coverString = "{" + coverString + "}";
			res += coverString + "\n";
		}
		return res;
	}
	
	@Override
	public String toString() {
		String res = "", row;
		for (int i = 0; i < 6; i++) {
			row = "";
			for (int j = 0; j < 7; j++) {
				row += ", " + coverMatrix[i][j].toString();
			}
			row = row.substring(2);
			row = "[" + row + "]";
			res += row + "\n";
		}
		return res;
	}

	public static void main(String[] args) {
		Problem pb = new Problem(7, tab1);

		System.out.println(pb);
		LinkedList<LinkedList<LinkedList<Integer>>> exactCoverUn = pb.exactCover1();
		LinkedList<LinkedList<LinkedList<Integer>>> exactCoverDeux = pb.exactCover2();
		LinkedList<LinkedList<LinkedList<Integer>>> exactCoverdl = pb.exactCoverdl();
		System.out.println(displayExactCover(exactCoverUn));
		System.out.println(displayExactCover(exactCoverDeux));
		System.out.println(displayExactCover(exactCoverdl));
	}

}
