import java.util.LinkedList;

public class InjectiveTiling extends Tiling {
	public LinkedList<Integer> indexRepresentants;

	public InjectiveTiling(LinkedList<Polyomino> polyominoSet, Polyomino surface) {
		super(polyominoSet, surface);
	}
	
	
	@Override
	public void init() {
		this.actualizePolyominoSet();
		this.buildC();
		this.buildTab();
		this.pb = new Problem(this.surface.nbPat+this.indexRepresentants.size(), tab);
	}
	

	public void actualizePolyominoSet() {
		LinkedList<Polyomino> temp = new LinkedList<Polyomino>();
		LinkedList<Integer> indexRepre = new LinkedList<Integer>();
		for (Polyomino poly : polyominoSet) {
			if (!temp.contains(poly)) {
				FreePoly fp = new FreePoly(poly);
				fp.generateRepresentants();
				temp.addAll(fp.representants);
				indexRepre.add(fp.representants.size());
			}
		}
		this.polyominoSet = temp;
		this.indexRepresentants = indexRepre;

	}
	
	@Override
	public void buildC() {
		this.C = new LinkedList<LinkedList<Integer>>();
		this.Cpoly = new LinkedList<Polyomino>();
		
		int nbFreePoly = this.indexRepresentants.size();
		int p = surface.nbPat;
		
		// chaque �l�ment de C correspond � un polyomino � un endroit pr�cis
		// parcours des polyominos
		int sumNbPoly = 0;
		for (int numFreePoly = 0 ; numFreePoly < nbFreePoly ; numFreePoly++) {
			int nbFixedPoly = this.indexRepresentants.get(numFreePoly);
			for (int i = sumNbPoly; i < sumNbPoly+nbFixedPoly; i++) {
				Polyomino poly = this.polyominoSet.get(i);
				Coord postionInitiale = poly.coordinates.get(0);
				for (Coord tile : surface.coordinates) {
					LinkedList<Integer> subset = new LinkedList<Integer>(); // subset in building
					poly.setPosition(tile);
					if (!poly.contenuDans(surface)) {
						continue; // on passe au suivant
					} else {
						for (Coord coord : poly.coordinates) {
							subset.add(this.tileIndex[coord.x][coord.y]+nbFreePoly);
						}
						// ici �tape cruciale, on ajoute aux ensemble le num�ro du free polyomino
						subset.add(numFreePoly);
						this.C.add(subset);
						this.Cpoly.add(poly.clone());
					}
				}
				// pour �viter les effets de bord
				poly.setPosition(postionInitiale);
			}
			sumNbPoly += nbFixedPoly;
		}
		System.err.println("C built : size = " + this.C.size());
	}

	public static void main(String[] args) {
		Polyomino surface = Polyomino.bigSquare(8);
		surface.recentre();
		for (int i =3 ; i < 5 ; i++)
			for (int j = 3 ; j<5;j++)
				surface.remove(new Coord(i,j));
		InjectiveTiling tiling = new InjectiveTiling(GeneratePoly.generatePoly(5), surface);
		tiling.polyCover();		
		Image2d img = new Image2d(220, 220);
		Image2dViewer view = new Image2dViewer(img);
		for (Polyomino poly : tiling.covers.get(0)) {
			poly.draw(img);
		}

		
	}

}
