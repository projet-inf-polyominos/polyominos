import java.util.LinkedList;

public class Performances {

	public static void exactCover() {
		// exact cover
		Polyomino surface = Polyomino.bigSquare(8);
		surface.recentre();
		for (int i = 3; i < 5; i++)
			for (int j = 3; j < 5; j++)
				surface.remove(new Coord(i, j));
		InjectiveTiling tiling = new InjectiveTiling(GeneratePoly.generatePoly(5), surface);
		Problem pb = tiling.pb;
		Chrono chrono = new Chrono();
		// Version 1 sans l'heuristique
		chrono.start();
		LinkedList<LinkedList<LinkedList<Integer>>> exactCoverUn = pb.exactCover1();
		chrono.stop();
		long dureeV1 = chrono.getDureeMs();
		System.out.println(dureeV1);
		// Version 2 avec l'heuristique
		chrono.start();
		LinkedList<LinkedList<LinkedList<Integer>>> exactCoverDeux = pb.exactCover2();
		chrono.stop();
		long dureeV2 = chrono.getDureeMs();
		System.out.println(dureeV2);
		// avec l'algo des dancing links de Knuth
		chrono.start();
		LinkedList<LinkedList<LinkedList<Integer>>> exactCoverdl = pb.exactCoverdl();
		chrono.stop();
		long dureedl = chrono.getDureeMs();
		System.out.println(dureedl);
	}

	public static void generation() {
		Chrono chrono = new Chrono();
		LinkedList<Polyomino> l = new LinkedList<Polyomino>();

		for (int i = 1; i < 10; i++) {
			System.out.println("n = " + i);
			chrono.start();
			l = GenerateNaive2.generateAllFixedPolyominoes2(i);
			chrono.stop();
			System.out.println("	" + chrono.getDureeMs());

			chrono.start();
			l = GeneratePoly.generatePoly(i);
			GenerateNaive.filtreAire(l, i);
			chrono.stop();
			System.out.println("	" + chrono.getDureeMs());
		}

		for (int i = 10; i < 12; i++) {
			System.out.println("n = " + i);
			chrono.start();
			l = GeneratePoly.generatePoly(i);
			GenerateNaive.filtreAire(l, i);
			chrono.stop();
			System.out.println("	" + chrono.getDureeMs());
		}
	}

	public static void generationFree() {
		Chrono chrono = new Chrono();
		LinkedList<Polyomino> l = new LinkedList<Polyomino>();

		for (int i = 1; i < 11; i++) {
			chrono.start();
			l = GeneratePoly.generatePoly(i);
			FreePoly.filtreFreePolyominoes(l);
			chrono.stop();
			System.out.println("n = "+i+", t = "+chrono.getDureeMs());
		}

	}

	public static void main(String[] args) {
		generationFree();
	}

}
