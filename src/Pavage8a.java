import java.util.LinkedList;

public class Pavage8a {

	public static final Integer[][] paralellogramme = new Integer[][] { { 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0 }, { 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0 },
			{ 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0 }, { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0 },
			{ 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }, { 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1 },
			{ 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1 }, { 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1 }, };

	public static final Integer[][] losange = new Integer[][] { { 0, 0, 0, 0, 1, 1, 0, 0, 0, 0 },
			{ 0, 0, 0, 1, 1, 1, 1, 0, 0, 0 }, { 0, 0, 1, 1, 1, 1, 1, 1, 0, 0 }, { 0, 1, 1, 1, 1, 1, 1, 1, 1, 0 },
			{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }, { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }, { 0, 1, 1, 1, 1, 1, 1, 1, 1, 0 },
			{ 0, 0, 1, 1, 1, 1, 1, 1, 0, 0 }, { 0, 0, 0, 1, 1, 1, 1, 0, 0, 0 }, { 0, 0, 0, 0, 1, 1, 0, 0, 0, 0 }, };

	public static final Integer[][] triangle = new Integer[][] { { 0, 0, 0, 0, 1, 1, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 1, 1, 0, 0, 0, 0 }, { 0, 0, 0, 1, 1, 1, 1, 0, 0, 0 }, { 0, 0, 0, 1, 1, 1, 1, 0, 0, 0 },
			{ 0, 0, 1, 1, 1, 1, 1, 1, 0, 0 }, { 0, 0, 1, 1, 1, 1, 1, 1, 0, 0 }, { 0, 1, 1, 1, 1, 1, 1, 1, 1, 0 },
			{ 0, 1, 1, 1, 1, 1, 1, 1, 1, 0 }, { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }, { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 } };

	public final static Polyomino poly1 = new Polyomino(
			"[(0,0), (0,1), (1,0), (1,1), (1,2), (1,3), (2,0), (2,1), (2,2), (2,3), (2,4), (2,5), (3,0), (3,1), (3,2), (3,3), (3,4), (3,5), (3,6), (3,7), (4,0), (4,1), (4,2), (4,3), (4,4), (4,5), (4,6), (4,7), (4,8), (4,9), (5,1), (5,2), (5,3), (5,4), (5,5), (5,6), (5,7), (5,8), (5,9), (5,10), (6,3), (6,4), (6,5), (6,6), (6,7), (6,8), (6,9), (6,10), (7,5), (7,6), (7,7), (7,8), (7,9), (7,10), (8,7), (8,8), (8,9), (8,10), (9,9), (9,10)]");
	public final static Polyomino poly2 = new Polyomino(
			"[(0,4), (0,5), (1,3), (1,4), (1,5), (1,6), (2,2), (2,3), (2,4), (2,5), (2,6), (2,7), (3,1), (3,2), (3,3), (3,4), (3,5), (3,6), (3,7), (3,8), (4,0), (4,1), (4,2), (4,3), (4,4), (4,5), (4,6), (4,7), (4,8), (4,9), (5,0), (5,1), (5,2), (5,3), (5,4), (5,5), (5,6), (5,7), (5,8), (5,9), (6,1), (6,2), (6,3), (6,4), (6,5), (6,6), (6,7), (6,8), (7,2), (7,3), (7,4), (7,5), (7,6), (7,7), (8,3), (8,4), (8,5), (8,6), (9,4), (9,5)]");
	public final static Polyomino poly3 = new Polyomino(
			"[(0,4), (0,5), (1,4), (1,5), (2,3), (2,4), (2,5), (2,6), (3,3), (3,4), (3,5), (3,6), (4,2), (4,3), (4,4), (4,5), (4,6), (4,7), (5,2), (5,3), (5,4), (5,5), (5,6), (5,7), (6,1), (6,2), (6,3), (6,4), (6,5), (6,6), (6,7), (6,8), (7,1), (7,2), (7,3), (7,4), (7,5), (7,6), (7,7), (7,8), (8,0), (8,1), (8,2), (8,3), (8,4), (8,5), (8,6), (8,7), (8,8), (8,9), (9,0), (9,1), (9,2), (9,3), (9,4), (9,5), (9,6), (9,7), (9,8), (9,9)]");

	public final static Polyomino[] polys = new Polyomino[] { poly1, poly2, poly3 };

	public static void buildShapes() {
		LinkedList<Coord> coordinatesPara = new LinkedList<Coord>();
		for (int i = 0; i < paralellogramme.length; i++) {
			for (int j = 0; j < paralellogramme[0].length; j++) {
				if (paralellogramme[i][j] == 1)
					coordinatesPara.add(new Coord(i, j));
			}
		}
		LinkedList<Coord> coordinatesLos = new LinkedList<Coord>();
		for (int i = 0; i < losange.length; i++) {
			for (int j = 0; j < losange[0].length; j++) {
				if (losange[i][j] == 1)
					coordinatesLos.add(new Coord(i, j));
			}
		}
		LinkedList<Coord> coordinatesTri = new LinkedList<Coord>();
		for (int i = 0; i < triangle.length; i++) {
			for (int j = 0; j < triangle[0].length; j++) {
				if (triangle[i][j] == 1)
					coordinatesTri.add(new Coord(i, j));
			}
		}
		System.out.println(new Polyomino(coordinatesPara));
		System.out.println(new Polyomino(coordinatesLos));
		System.out.println(new Polyomino(coordinatesTri));
	}

	public static void main(String[] args) {
		for (Polyomino polyi : polys) {
			GeneratePoly genp = new GeneratePoly(5);
			genp.generateFixedPoly();
			LinkedList<Polyomino> list = genp.fixedPolyList;
			GenerateNaive.filtreAire(list, 5);
			for (Polyomino poly : list) {
				poly.recentre();
			}
			;
			// System.out.println(surface);

			InjectiveTiling tiling = new InjectiveTiling(list, polyi);
			tiling.polyCover();
			Image2d img = new Image2d(230, 230);
			Image2dViewer view = new Image2dViewer(img);
			if (tiling.covers.size()>0)
				for (Polyomino poly : tiling.covers.get(0)) {
					poly.draw(img);
				}
		}
	}
}
