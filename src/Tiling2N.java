import java.util.HashMap;
import java.util.LinkedList;

public class Tiling2N extends Tiling {
	public final static int N = 13;

	public Tiling2N(LinkedList<Polyomino> polyominoSet, Polyomino surface) {
		super(polyominoSet, surface);
	}

	@Override
	public void init() {
		this.buildC();
		this.buildTab();
		this.pb = new Problem(this.surface.nbPat + this.nbPoly, tab);
	}

	@Override
	public void buildC() {
		this.C = new LinkedList<LinkedList<Integer>>();
		this.Cpoly = new LinkedList<Polyomino>();
		// chaque �l�ment de C correspond � un polyomino � un endroit pr�cis
		// parcours des polyominos

		int i = 0;
		for (Polyomino poly : this.polyominoSet) {
			Coord postionInitiale = poly.coordinates.get(0);
			for (Coord tile : surface.coordinates) {
				LinkedList<Integer> subset = new LinkedList<Integer>(); // subset in building
				poly.setPosition(tile);
				if (!poly.contenuDans(surface)) {
					continue; // on passe au suivant
				} else {
					for (Coord coord : poly.coordinates) {
						subset.add(this.tileIndex[coord.x][coord.y] + this.nbPoly);
					}
					subset.add(i);
					this.C.add(subset);
					this.Cpoly.add(poly.clone());
				}
			}
			// pour �viter les effets de bord
			poly.setPosition(postionInitiale);
			i++;
		}
		System.err.println("C built : size = " + this.C.size());
	}

	public static LinkedList<Polyomino> generationDesOminos(int n) {
		LinkedList<Polyomino> res = new LinkedList<Polyomino>();
		for (int x = 1; x < n + 1; x++) {
			Polyomino temp = new Polyomino(new Coord());
			temp.add(new Coord(x, 0));
			res.add(temp);
		}
		return res;
	}

	public static Tiling2N pavage(int n, LinkedList<Polyomino> list) {
		Polyomino surface = Polyomino.rectangle(1, 2 * n);
		Tiling2N tiling = new Tiling2N(list, surface);
		tiling.polyCover();
		return tiling;
	}

	public static void main(String[] args) {
		HashMap<Integer, Integer> res = new HashMap<Integer, Integer>();
		Image2d img = new Image2d(200, 200);
		Image2dViewer view = new Image2dViewer(img);
		for (int n = 1; n < N; n++) {
			LinkedList<Polyomino> list = generationDesOminos(n);
			Tiling2N tiling = pavage(n, list);
			res.put(n, tiling.covers.size());
			if (tiling.covers.size() > 0) {
				for (Polyomino poly : tiling.covers.get(0)) {
					poly.translate(0, 2 * n - 1);
					poly.draw(img);
				}
			} else {
				new Polyomino(new Coord(0, 2*n-1)).draw(img);
			}
		}
		for (int i = 1; i < N; i++) {
			String s = "n = " + i + ", ";
			s += res.get(i) + " couvertures trouv�es";
			System.out.println(s);
		}
	}
}
