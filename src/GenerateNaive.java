import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;

public class GenerateNaive {

	/**
	 * Pour la g�n�ration na�ve des polyominos d'aire donn�e n, on cr�e tous les
	 * polyominos compris dans un carr� nxn, on ne garde que ceux de taille n puis
	 * on ne garde qu'un seul repr�sentant par classe
	 */
	public static HashSet<Polyomino> generateAllFixedPolyominoes(int n) {
		Polyomino surface = Polyomino.bigSquare(n);
		HashSet<Polyomino> res = generateAllFixedPolyominoes(surface);
		filtreAire(res, n);
		res = quotientTranslation(res);
		return res;
	}

	/**
	 * Ne garde que les polyominoes de taille n
	 */
	public static void filtreAire(Collection<Polyomino> hset, int n) {
		Object[] temp = hset.toArray();
		for (Object pol : temp) {
			Polyomino poly = (Polyomino) pol;
			if (poly.nbPat != n)
				hset.remove(poly);
		}
	}

	/**
	 * Pour obtenir un repr�sentant des "fixed polyominoes" sachant que tout les
	 * repr�sentants sont dans l'ensemble, il suffit de ne garder que celui qui
	 * s'appuie contre le bord sup�rieur droit
	 */
	public static HashSet<Polyomino> quotientTranslation(HashSet<Polyomino> hset) {
		HashSet<Polyomino> res = new HashSet<Polyomino>();
		for (Polyomino poly : hset) {
			boolean onWestEdge = false, onNorthEdge = false;
			for (Coord coord : poly.coordinates) {
				if (coord.x == 0)
					onNorthEdge = true;
				if (coord.y == 0)
					onWestEdge = true;
			}
			if (onNorthEdge && onWestEdge)
				res.add(poly);
		}
		return res;
	}


	/**
	 * Pour la g�n�ration na�ve des polyominos d'une surface, on parcourt toutes les
	 * cases de surface et pour chacune on fait pousser les polyominos autour 
	 * Les cases de d�part sont appel�es les origines De cette mani�re la connexit� des
	 * polyominos n'a pas besoin d'�tre v�rifi�e
	 */
	public static HashSet<Polyomino> generateAllFixedPolyominoes(Polyomino surface) {
		HashSet<Polyomino> res = new HashSet<Polyomino>();
		for (int indOrigin = 0; indOrigin < surface.nbPat; indOrigin++)
			res.addAll(generateAllFixedPolyominoes(surface, indOrigin));
		return res;
	}

	/**
	 * Renvoie l'ensemble des polyominos inclus dans surface contenant origin =
	 * surface.get(indOrigin) 
	 * Invariant : � l'appel, origin est dans surface mais la
	 * surface n'est plus forc�ment un polyomino
	 */
	public static HashSet<Polyomino> generateAllFixedPolyominoes(Polyomino surface, int indOrigin) {
		Coord origin = surface.get(indOrigin);
		HashSet<Polyomino> res = new HashSet<Polyomino>();
		res.add(new Polyomino(origin.clone()));
		// initialisation
		if (surface.nbPat == 1) {
			return res;
		}
		// Si la surface n'est pas trivial, on enl�ve l'origine
		// et on g�n�re les polyominos dans toutes les directions tout autour
		// avant de les fusionner
		surface.remove(indOrigin);
		for (int i = 0; i < 4; i++) {
			Coord dir = Polyomino.DIRS[i];
			Coord tile = Coord.sum(origin, dir);
			if (surface.coordinates.contains(tile)) {
				int indTile = surface.coordinates.indexOf(tile);
				res = merge(res, assemble(generateAllFixedPolyominoes(surface, indTile), origin));
			}
		}
		// backtracking
		surface.add(indOrigin, origin);
		return res;
	}

	/**
	 * assuming the polyominoes are strictly neighbors
	 */
	private static HashSet<Polyomino> merge(HashSet<Polyomino> set1, HashSet<Polyomino> set2) {
		HashSet<Polyomino> res = new HashSet<Polyomino>();
		res.addAll(set1);
		res.addAll(set2);
		for (Polyomino poly1 : set1) {
			for (Polyomino poly2 : set2) {
				res.add(Polyomino.merge(poly1, poly2));
			}
		}
		return res;
	}

	/**
	 * Renvoie une nouvelle liste contenant tous les poylominos de l'argument augment�s
	 * de tile, (en supposant que cette op�ration g�n�re toujours des polyominos)
	 */
	private static HashSet<Polyomino> assemble(HashSet<Polyomino> listPoly, Coord tile) {
		HashSet<Polyomino> res = new HashSet<Polyomino>();
		for (Polyomino polyomino : listPoly) {
			res.add(polyomino.clone());
		}
		for (Polyomino polyomino : res) {
			polyomino.add(tile);
		}
		res.add(new Polyomino(tile));
		return res;
	}

	public static void main(String[] args) {
		HashSet<Polyomino> l = generateAllFixedPolyominoes(Polyomino.bigSquare(2));
		l = quotientTranslation(l);
		Image2d img = new Image2d(900, 100);
		Image2dViewer view = new Image2dViewer(img);
		int i = 0;
		for (Polyomino poly : l) {
			Polyomino temp = poly.clone();
				
			temp.translate(2+ i*5, 2);
			temp.draw(img);
			i++;
			System.out.println(poly);
		}
	}

}
