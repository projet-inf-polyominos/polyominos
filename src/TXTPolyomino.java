import java.util.LinkedList;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Collections;
import java.awt.Color;

public class TXTPolyomino {
	public static LinkedList<Polyomino> polyominoFromTxt(String path) {
		Path wiki_path = Paths.get(path);
		LinkedList<Polyomino> polyominoes = new LinkedList<Polyomino>();
		Charset charset = Charset.forName("ISO-8859-1");

		try {
			List<String> lines = Files.readAllLines(wiki_path, charset);
			// on lit les polyominos du texte, un par ligne
			for (String line : lines) {
				System.out.println(line);
				polyominoes.add(new Polyomino(readPolyomino(line)));
			}
		} catch (IOException e) {
			System.out.println(e);
		}
		return polyominoes;
	}

	public static LinkedList<Coord> readPolyomino(String line) {
		// carres du polyomino actuel donne par la ligne du texte
		LinkedList<Coord> coordinates = new LinkedList<Coord>();

		double x = 0;
		double y = 0;

		// increment
		int k_x = 0;
		int k_y = 0;

		boolean reading_x = true;

		for (int i = 0; i < line.length(); i++) {
			char c = line.charAt(i);

			// fin de lecture du carre
			if (c == ')') {
				x = x * Math.pow(10, k_x - 1);
				y = y * Math.pow(10, k_y - 1);
				coordinates.add(new Coord((int) x, (int) y));
				x = 0;
				y = 0;
				k_x = 0;
				k_y = 0;
			}

			// on passe a y ou bien virgule inutile
			if (c == ',') {
				reading_x = false;
			}

			if (c == '(') {
				reading_x = true;
			}

			if (Character.isDigit(c)) {
				if (reading_x) {
					x += Character.getNumericValue(c) * Math.pow(10, -k_x);
					k_x += 1;
				} else {
					// on lit "a lenvers"
					y += Character.getNumericValue(c) * Math.pow(10, -k_y);
					k_y += 1;
				}
			}
		}
		return coordinates;
	}

}
