import java.util.LinkedList;

public class ExactCover {

	/**
	 * Impl�mentation de l'algorithme de la couverture exacte en toute g�n�ralit�
	 * 
	 * @param X L'ensemble � couvrir
	 * @param C Une liste de sous-ensemble de X � utiliser pour la couverture exacte
	 *          C est suppos� non vide
	 * @return la liste de tous les couvertures possibles o� une couverture est une
	 *         liste de sous-ensemble de X partitionnant x
	 */
	@SuppressWarnings("unchecked")
	public static <T> LinkedList<LinkedList<LinkedList<T>>> findExactCoverVer1(LinkedList<T> X, LinkedList<LinkedList<T>> C) {
		LinkedList<LinkedList<LinkedList<T>>> covers = new LinkedList<LinkedList<LinkedList<T>>>();
		if (!X.isEmpty()) {
			T x = X.get(0);
			for (LinkedList<T> S : C) {
				if (S.contains(x)) {
					LinkedList<T> Xprime = (LinkedList<T>) X.clone();
					// copie de C : attention aux references... on copie tous les sous_ensembles
					LinkedList<LinkedList<T>> Cprime = new LinkedList<LinkedList<T>>();
					for (LinkedList<T> U : C) {
						Cprime.add((LinkedList<T>) U.clone());
					}
					for (T y : S) {
						Xprime.remove(y);
						for (LinkedList<T> T : C) {
							if (T.contains(y)) {
								Cprime.remove(T);
							}
						}
					}

					// si Xprime est vide c'est que S est une couverture de X
					if (Xprime.isEmpty()) {
						LinkedList<LinkedList<T>> temp = new LinkedList<LinkedList<T>>();
						temp.add(S);
						covers.add(temp);
					} else {
						for (LinkedList<LinkedList<T>> P : findExactCoverVer1(Xprime, Cprime)) {
							P.add(S);
							covers.add(P);
						}
					}

				}
			}
		}
		return covers;
	}

	/**
	 * Reprend l'algorithme pr�c�dent en utilisant l'heuristique sugg�r�e on prend x
	 * minimisant le nombre d'�l�ment de C le contenant
	 */
	public static <T> LinkedList<LinkedList<LinkedList<T>>> findExactCoverVer2(LinkedList<T> X, LinkedList<LinkedList<T>> C) {
		LinkedList<LinkedList<LinkedList<T>>> covers = new LinkedList<LinkedList<LinkedList<T>>>();
		if (!X.isEmpty()) {
			T x = getBestx(X, C);
			for (LinkedList<T> S : C) {
				if (S.contains(x)) {
					LinkedList<T> Xprime = (LinkedList<T>) X.clone();
					// copie de C : attention aux references... on copie tous les sous_ensembles
					LinkedList<LinkedList<T>> Cprime = new LinkedList<LinkedList<T>>();
					for (LinkedList<T> U : C) {
						Cprime.add((LinkedList<T>) U.clone());
					}
					for (T y : S) {
						Xprime.remove(y);
						for (LinkedList<T> T : C) {
							if (T.contains(y)) {
								Cprime.remove(T);
							}
						}
					}

					// si Xprime est vide c'est que S est une couverture de X
					if (Xprime.isEmpty()) {
						LinkedList<LinkedList<T>> temp = new LinkedList<LinkedList<T>>();
						temp.add(S);
						covers.add(temp);
					} else {
						for (LinkedList<LinkedList<T>> P : findExactCoverVer2(Xprime, Cprime)) {
							P.add(S);
							covers.add(P);
						}
					}

				}
			}
		}
		return covers;
	}

	public static <T> T getBestx(LinkedList<T> X, LinkedList<LinkedList<T>> C) {
		// test initial
		int n = X.size();
		if (n == 1)
			return X.get(0);
		// initialisation
		T temp = X.get(0);
		int occTemp = 0;
		for (LinkedList<T> set : C) {
			if (set.contains(temp))
				occTemp++;
		}
		T champion = temp;
		int occChampion = occTemp;
		// parcours des champions
		for (int i = 1; i < n; i++) {
			temp = X.get(i);
			// comptage des occurences
			for (LinkedList<T> set : C) {
				if (set.contains(temp))
					occTemp++;
			}
			if (occChampion > occTemp) {
				champion = temp;
				occChampion = occTemp;
			}
		}
		return champion;
	}
}
