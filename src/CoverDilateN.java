import java.util.LinkedList;

public class CoverDilateN {
	public static final int n = 8;
	public static final int alpha = 4;
	
	public static void main(String[] args) {
		LinkedList<Polyomino> list = GeneratePoly.generatePoly(n);
		GenerateNaive.filtreAire(list, n);
		FreePoly.filtreFreePolyominoes(list);
		for (Polyomino pattern : list) {

			Polyomino surface = pattern.clone();
			surface.rotation(0, 0);
			surface.dilatation(alpha);
			surface.recentre();

			FreePoly fp = new FreePoly(pattern.clone());
			fp.generateRepresentants();

			LinkedList<Polyomino> polyominoSet = new LinkedList<Polyomino>();
			for (Polyomino poly : fp.representants) {
				polyominoSet.add(poly);
			}
			for (Polyomino poly : polyominoSet) {
				poly.recentre();
			}

			Tiling tiling = new Tiling(polyominoSet, surface);

			LinkedList<LinkedList<Polyomino>> covers = tiling.polyCover();
			if (covers.size() > 0) {
				Image2d img = new Image2d(80, 80);
				Image2dViewer view = new Image2dViewer(img);

				for (Polyomino poly : covers.get(0)) {
					poly.draw(img);
				}
			}

		}
		System.out.println("Terminated");
	}

}
