package dancing_links;

import java.util.LinkedList;

public class DancingLinks {
	public Boolean[][] coverMatrix;
	// pour garder la position des objets
	public DataObject[][] dataObjects;
	// liste des 1
	public LinkedList<Column> columns;
	public DataObject H;

	
	// format de la matrice : M_{S, x} avec x dans X et S dans C
	// chaque ligne d�crit un sous ensemble de C
	public DancingLinks(Boolean[][] coverMatrix) {
		this.coverMatrix = coverMatrix;
		// n = nombre de lignes, m = nombre de colonnes
		int n = coverMatrix.length;
		int m = coverMatrix[0].length;

		// on doit garder la position des 1 dans la matrice : on opte donc pour le
		// format "matrice" pour les dataObjects
		this.dataObjects = new DataObject[n][m];
		this.columns = new LinkedList<Column>();
		this.H = new Column(null, null, null, null, 0, "H");
		initMatrices();
		System.err.println("Matrix built");
		System.err.println("Dimension : height = "+coverMatrix.length+" , width = " + coverMatrix[0].length);
	}


	// fonction d'initialisation
	public void initMatrices() {
		int n = coverMatrix.length; // nbLignes
		int m = coverMatrix[0].length; // nbColonnes

		// init des colonnes
		for (int j = 0; j < m; j++) {
			this.columns.add(new Column(null, null, null, null, 0, String.valueOf(j)));
		}

		// initialisation de la matrice de DataObject
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				DataObject obj = new DataObject(null, null, null, null, i, columns.get(j));
				this.dataObjects[i][j] = obj;
			}
		}

		// remplissage des column headers
		for (int i = 1; i < m - 1; i++) {
			Column col = this.columns.get(i);
			col.R = this.columns.get((i + 1) % m);
			col.L = this.columns.get(Math.floorMod(i - 1, m));
		}
		// on relie H egalement aux colonnes aux extremites
		this.columns.get(0).L = this.H;
		this.columns.get(0).R = this.columns.get(1);
		this.columns.get(m - 1).R = this.H;
		this.columns.get(m - 1).L = this.columns.get(m - 2);
		H.L = this.columns.get(m - 1);
		H.R = this.columns.get(0);

		// detection des voisins
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				// on tombe sur un true
				if (coverMatrix[i][j]) {
					this.columns.get(j).S++;
					this.setNeighbours(i, j);
				}
			}
		}
		
		// si aucun 1 dans la colonne, les up et down n'ont pas �t� actualis�s...
		// il faut le faire, sinon il y a des nullPointerExceptions !
		for(int j = 0; j < m; j++) {
			if(this.columns.get(j).U == null) {
				this.columns.get(j).U = this.columns.get(j);
			}
			if(this.columns.get(j).D == null) {
				this.columns.get(j).D = this.columns.get(j);
			}
		}
	}
	
	// coordonnees i et j du 1
		public void setNeighbours(int i, int j) {
			DataObject obj = dataObjects[i][j];
			Boolean[] line = coverMatrix[i];

			// Boolean[] column = this.coverMatrix.getColumn(j);
			Boolean[] column = new Boolean[coverMatrix.length];

			for (int a = 0; a < coverMatrix.length; a++) {
				column[a] = coverMatrix[a][j];
			}

			// get R
			for (int k = 1; k <= line.length; k++) {
				// premier 1 rencontre a droite
				int l = (j + k) % line.length;
				if (line[l]) {
					obj.R = dataObjects[i][l];
					break;
				}
			}

			// get L
			for (int k = 1; k <= line.length; k++) {
				// premier 1 rencontre a gauche
				// attention au signe
				int m = Math.floorMod(j - k, line.length);
				if (line[m]) {
					obj.L = dataObjects[i][m];
					break;
				}
			}

			// get D
			for (int k = 1; k <= column.length; k++) {
				// premier 1 rencontre en bas : si pas de 1, alors vaut la colonne
				int l = (i + k) % column.length;
				if (l != 0 && column[l]) {
					obj.D = dataObjects[l][j];
					break;
				}
				// pas de 1 plus bas
				if (l == 0) {
					obj.D = this.columns.get(j);
					this.columns.get(j).U = obj;
					break;
				}
			}

			// get U
			for (int k = 1; k <= i + 1; k++) {
				// premier 1 rencontre en haut : si on ne trouve pas de 1, alors vaut la colonne
				int m = (i - k);
				if (m >= 0 && column[m]) {
					obj.U = dataObjects[m][j];
					break;
				}
				// on a depasse le haut de la colonne
				if (m < 0) {
					obj.U = this.columns.get(j);
					this.columns.get(j).D = obj;
					break;
				}
			}
		}
	
		public LinkedList<LinkedList<Integer>> indexExactCover(){
			LinkedList<LinkedList<Integer>> res = this.indexExactCoverRec();
			System.err.println("Dancing links algorithm done, "+res.size()+" covers found" );
			return res;
		}

	// on renvoie la liste des indices des sous-ensembles a utiliser
	// pour couvrir l'ensemble de base
	// donc liste de liste d'entiers pour avoir toutes les solutions
	public LinkedList<LinkedList<Integer>> indexExactCoverRec() {
		// plus de colonnes a couvrir
		if (this.H.R == H) {
			LinkedList<LinkedList<Integer>> finalP = new LinkedList<LinkedList<Integer>>();
			// ensemble vide
			finalP.add(new LinkedList<Integer>());
			return finalP;
		}
		// ensemble d'exact covers a renvoyer
		LinkedList<LinkedList<Integer>> P = new LinkedList<LinkedList<Integer>>();
		DataObject x = this.getMinimalSizeColumn();
		this.coverColumn(x);
		DataObject t = x.U;
		DataObject y;

		// equivalent a t = x
		while (t.C != t) {
			y = t.L;
			while (y != t) {
				this.coverColumn(y.C);
				y = y.L;
			}
			// appel recursif
			LinkedList<LinkedList<Integer>> P_rec = this.indexExactCoverRec();
			// on a trouve une exact cover recursivement !
			if (P_rec.size() > 0) {
				for (LinkedList<Integer> p : P_rec) {
					p.add(t.subset);
					P.add(p);
				}
			}
			y = t.R;
			while (y != t) {
				this.uncoverColumn(y.C);
				y = y.R;
			}
			t = t.U;
		}
		this.uncoverColumn(x.C);
		return P;
	}

	// couvre la colonne numero n
	public void coverColumn(DataObject x) {
		x.R.L = x.L;
		x.L.R = x.R;
		DataObject t = x.D;
		DataObject y;
		// on ne peut pas utiliser l'egalite entre les elements DataObject et colonne
		// il faut ruser.. on teste l'egalite x = t avec colonne de t = t
		while (t.C != t) {
			y = t.R;
			while (y != t) {
				y.D.U = y.U;
				y.U.D = y.D;
				y.C.S -= 1;
				y = y.R;
			}
			t = t.D;
		}
	}

	// d�couvre la colonne numero n
	public void uncoverColumn(DataObject x) {
		x.R.L = x;
		x.L.R = x;
		DataObject t = x.U;
		DataObject y;
		while (t.C != t) {
			y = t.L;
			while (y != t) {
				y.D.U = y;
				y.U.D = y;
				y.C.S += 1;
				y = y.L;
			}
			t = t.U;
		}
	}

	// retourne une colonne de taille minimale parmi les colonnes non encore
	// couvrees
	public DataObject getMinimalSizeColumn() {
		DataObject x = this.H.R;
		DataObject xMin = x;
		int min = x.C.S;
		while (x != H) {
			if (x.C.S < min) {
				min = x.C.S;
				xMin = x;
			}
			x = x.R;
		}
		return xMin;
	}

}
