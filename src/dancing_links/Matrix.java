package dancing_links;


import java.util.LinkedList;

public class Matrix<T> extends LinkedList<LinkedList<T>>{
	LinkedList<LinkedList<T>> matrix;
	
	public Matrix() {
		this.matrix = new LinkedList<LinkedList<T>>();
	}
	
	public LinkedList<T> getColumn(int i) {
		LinkedList<T> Column = new LinkedList<T>();
		for(int j = 0; j < this.matrix.size(); j++) {
			Column.add(this.matrix.get(j).get(i));
		}
		return Column;
	}
	
	public int size() {
		return super.size();
	}
	
	public LinkedList<T> getRow(int i) {
		return super.get(i);
	}

}
