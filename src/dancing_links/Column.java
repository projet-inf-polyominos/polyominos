package dancing_links;

public class Column extends DataObject{
	public int S;// number of one in the column
	public String N;// name of the column
	
	public Column(DataObject U, DataObject D, DataObject L, DataObject R, int S, String N) {
		super(U, D, L, R, -1, null);
		this.C = this;
		this.N = N;
		this.S = S;
	}
}
