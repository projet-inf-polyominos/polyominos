package dancing_links;

public class DataObject {
	public DataObject U;
	public DataObject D;
	public DataObject L;
	public DataObject R;
	public Column C;
	// index de l'ensemble correspondant
	public int subset;
	
	public DataObject(DataObject U, DataObject D, DataObject L, DataObject R, int n, Column C) {
		this.U = U;
		this.D = D;
		this.L = L;
		this.R = R;
		this.C = C;
		this.subset = n;
	}
	
	
}
