import java.util.LinkedList;
import java.util.HashSet;

import dancing_links.DancingLinks;

public class Tiling { // classe de conversion
	public LinkedList<Polyomino> polyominoSet; // les polyominos sont recentr�s
	public int nbPoly;
	// se convertit en
	public LinkedList<LinkedList<Integer>> C;
	public LinkedList<Polyomino> Cpoly; // �quivalent de C mais avec des
	// polyominos pour pouvoir remonter plus facilement
	public int[][] tab; // traduction de C pour la classe Problem

	public Polyomino surface; // � paver
	public LinkedList<Integer> X; // on se ram�ne � la couverture de [0,p[

	public int[][] tileIndex; // fais le lien entre la position de la case et son num�ro

	public Problem pb;
	public LinkedList<LinkedList<Polyomino>> covers;

	public Tiling(LinkedList<Polyomino> polyominoSet, Polyomino surface) {
		this.covers = null;
		this.polyominoSet = polyominoSet;
		this.nbPoly = polyominoSet.size();
		this.surface = surface;
		this.buildTileIndex();
		this.buildX();
		this.init();
	}
	
	public void init() {
		this.buildC();
		this.buildTab();
		this.pb = new Problem(this.surface.nbPat, tab);	
	}


	public void buildTileIndex() {
		Coord tileMatrixSize = surface.getMaxXAndY();
		int n = tileMatrixSize.x;
		int m = tileMatrixSize.y;
		this.tileIndex = new int[n + 1][m + 1];
		// on remplit la matrice de tiling
		// d'abord avec des -1 qui signifieront qu'il n'a pas de carre
		for (int i = 0; i <= n; i++) {
			for (int j = 0; j <= m; j++) {
				tileIndex[i][j] = -1;
			}
		}
		// num�rotation des case de la surface � paver
		for (int k = 0; k < surface.nbPat; k++) {
			Coord tile = surface.coordinates.get(k);
			this.tileIndex[tile.x][tile.y] = k;
		}
	}

	public void buildX() { // couverture de [1,p]
		this.X = new LinkedList<Integer>();
		for (int i = 0; i < nbPoly; i++)
			X.add(i);
	}

	public void buildC() {
		this.C = new LinkedList<LinkedList<Integer>>();
		this.Cpoly = new LinkedList<Polyomino>();
		// chaque �l�ment de C correspond � un polyomino � un endroit pr�cis
		// parcours des polyominos
		for (Polyomino poly : this.polyominoSet) {
			Coord postionInitiale = poly.coordinates.get(0);
			for (Coord tile : surface.coordinates) {
				LinkedList<Integer> subset = new LinkedList<Integer>(); // subset in building
				poly.setPosition(tile);
				if (!poly.contenuDans(surface)) {
					continue; // on passe au suivant
				} else {
					for (Coord coord : poly.coordinates) {
						subset.add(this.tileIndex[coord.x][coord.y]);
					}
					this.C.add(subset);
					this.Cpoly.add(poly.clone());
				}
			}
			// pour �viter les effets de bord
			poly.setPosition(postionInitiale);
		}
		System.err.println("C built : size = " + this.C.size());
	}

	public void buildTab() {
		int N = C.size();
		int[][] res = new int[N][];
		// initialisation � false
		for (int i = 0; i < N; i++) {
			LinkedList<Integer> subset = C.get(i);
			int localM = subset.size();
			res[i] = new int[localM];
			for (int j = 0; j < localM; j++) {
				res[i][j] = subset.get(j);
			}
		}
		this.tab = res;
	}

	public LinkedList<LinkedList<LinkedList<Integer>>> exactCoverdl() {
		return this.pb.exactCoverdl();
	}

	public LinkedList<LinkedList<Polyomino>> polyCover() {
		if (this.covers != null)
			return covers;
		LinkedList<LinkedList<Integer>> coversIndexs = pb.indexExactCover();
		LinkedList<LinkedList<Polyomino>> covers = new LinkedList<LinkedList<Polyomino>>();
		for (LinkedList<Integer> coverIndex : coversIndexs) {
			LinkedList<Polyomino> tempCover = new LinkedList<Polyomino>();
			for (int index : coverIndex) {
				tempCover.add(Cpoly.get(index));
			}
			covers.add(tempCover);
		}
		this.covers = covers;
		return covers;
	}

	public static void main(String[] args) {
		GeneratePoly genp = new GeneratePoly(3);
		genp.generateFixedPoly();
		LinkedList<Polyomino> list = genp.fixedPolyList;
		GenerateNaive.filtreAire(list, 3);
		for (Polyomino poly : list) {
			poly.recentre();
			//System.out.println(poly);
		}
		//System.out.println(list.size());
		Polyomino surface = Polyomino.bigSquare(6);
		//System.out.println(surface);
		
		Tiling tiling = new Tiling(list, surface);
		System.out.println(tiling.tab.length);
		tiling.polyCover();
		Image2d img = new Image2d(200, 200);
		Image2dViewer view = new Image2dViewer(img);
		for (Polyomino poly : tiling.covers.get(57)) {
			
			poly.draw(img);
		}
		
	}

}