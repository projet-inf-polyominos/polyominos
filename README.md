# Polyominos

Toutes les classes sont dans le package par défaut du dossier src, 
sauf l'implémentation de l'algorithme des dancing-links qui se trouvent dans
un package à part

## Classe TXTPolyomino
Prend en entrée le chemin vers un fichier texte et en ressort les polyominos du
fichier.
Contient en particulier un lecteur de String

## Coord
Pair d'entier

## Polyomino
Se construit à partir d'une ArrayList de Coord représentant les carrés des 
polyominos, peut également se construire à partir d'une chaîne de caractère

## GenerateNaive
Génère tous les polyominos d'aire donnée en passant par la génération de tous
les polyominos d'une surface donnée (en l'occurence ici le carré de côté n)

## GenerateNaive2
Génère tous les polyominos d'aire donnée par un algorithme plus naÏf que la 
version  précédente

## GeneratePoly
Génération de tous les free polyominos de taille plus petite que n avec la 
méthode de Redelmeijer

## ExactCover
Prend en entrée un ensemble X (ArrayList) et une liste C de sous ensemble (
Arraylist<ArrayList<>>) et renvoie toutes les couvertures de X par les éléments
de C

## Package dancing_links
Implémente l'algorithme de Knuth

Dans la classe DancingLinks, la constructeur prend en entrée une matrice de 
couverture et contient une méthode exactCover qui renvoie la liste des 
couverture exacte par l'algorithme de Knuth.

## Problem
Convertit un problème de couverture (encodée par l'ensemble X et la liste des
sous ensembles C) en une matrice de couverture.

## Tiling
Convertit un problème de pavage (ensemble de polyominos et surface à paver) en
un problème de couverture (ensemble X et liste de sous-ensemble C) qui sera 
alors transformé en matrice de couverture dansla classe problème

## CoverDilate
Résoud la couverture de la dilatation d'un pentamino par ce pentamino

## Chrono
Class récupérée sur https://fr.jeffprod.com/blog/2015/un-chronometre-en-java/
pour avoir le temps d'exécution d'un code

## Performance
Mesure le temps d'exécution des générations de polyominos

## ColoredPolygon
Gère exclusivement l'affichage des polyominos
